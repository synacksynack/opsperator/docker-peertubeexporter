#!/bin/sh

EXPORTER_PORT=${EXPORTER_PORT:-9113}

if test "$DEBUG"; then
    set -x
fi

if test -z "$PEERTUBE_URL"; then
    PEERTUBE_URL=http://127.0.0.1:9000
fi

if test -z "$DONT_WAIT"; then
    while ! curl -k $PEERTUBE_URL/api/v1/ping 2>/dev/null | grep pong >/dev/null
    do
	echo Waiting for "$host:$port"
	sleep 10
    done
fi
export EXPORTER_PORT PEERTUBE_URL

exec python -u /usr/src/app/peertube_exporter.py
