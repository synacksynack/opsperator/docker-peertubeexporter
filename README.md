# k8s Prometheus PeerTube Exporter

Build with:

```
$ make build
```

Build in OpenShift:

```
$ make ocbuild
```

Cleanup OpenShift assets:

```
$ make ocpurge
```
