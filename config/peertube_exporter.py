#!/usr/bin/env python

import os

if os.environ.get('EXPORTER_PORT') is not None:
    bind_port = os.environ.get('EXPORTER_PORT')
else:
    bind_port = 9113
if os.environ.get('PEERTUBE_URL') is not None:
    peertube_root = os.environ.get('PEERTUBE_URL')
else:
    peertube_root = 'http://localhost:9000'

peertube_url = peertube_root + '/api/v1/server/stats'

import json
import requests
import sys
import time

from prometheus_client import start_http_server
from prometheus_client.core import GaugeMetricFamily, CounterMetricFamily, REGISTRY

class JSONCollector(object):
    def collect(self):
        response = json.loads(requests.get(peertube_url).content.decode('UTF-8'))

        yield GaugeMetricFamily('peertube_local_videos_count',
                                'Peertube Local Videos Count',
                                int(response['totalLocalVideos']))
        yield GaugeMetricFamily('peertube_local_video_views_count',
                                'Peertube Local Videos Views',
                                int(response['totalLocalVideoViews']))
        yield GaugeMetricFamily('peertube_local_video_size_bytes',
                                'Peertube Local Videos Space Used in Bytes',
                                int(response['totalLocalVideoFilesSize']))
        yield GaugeMetricFamily('peertube_local_video_comments_count',
                                'Peertube Local Videos Comments Count',
                                int(response['totalLocalVideoComments']))
        yield GaugeMetricFamily('peertube_all_videos_count',
                                'Peertube Total Videos Count',
                                int(response['totalVideos']))
        yield GaugeMetricFamily('peertube_all_video_comments_count',
                                'Peertube Total Videos Comments Count',
                                int(response['totalVideoComments']))
        yield GaugeMetricFamily('peertube_users_count',
                                'Peertube Total Users Count',
                                int(response['totalUsers']))
        yield GaugeMetricFamily('peertube_active_users_daily_count',
                                'Peertube Total Users Active in the last day',
                                int(response['totalDailyActiveUsers']))
        yield GaugeMetricFamily('peertube_active_users_weekly_count',
                                'Peertube Total Users Active in the last week',
                                int(response['totalWeeklyActiveUsers']))
        yield GaugeMetricFamily('peertube_active_users_monthly_count',
                                'Peertube Total Users Active in the last month',
                                int(response['totalMonthlyActiveUsers']))
        yield GaugeMetricFamily('peertube_followers_count',
                                'Peertube Followers Instances Count',
                                int(response['totalInstanceFollowers']))
        yield GaugeMetricFamily('peertube_following_count',
                                'Peertube Following Instances Count',
                                int(response['totalInstanceFollowing']))
        yield GaugeMetricFamily('peertube_pub_processed_count',
                                'Peertube Processed Publications Count',
                                int(response['totalActivityPubMessagesProcessed']))
        yield GaugeMetricFamily('peertube_pub_processed_per_sec_avg',
                                'Peertube Average Publications Processed per Second',
                                int(response['activityPubMessagesProcessedPerSecond']))
        yield GaugeMetricFamily('peertube_pub_waiting_count',
                                'Peertube Waiting Publications Count',
                                int(response['totalActivityPubMessagesWaiting']))

if __name__ == "__main__":
    REGISTRY.register(JSONCollector())
    start_http_server(int(bind_port))
    print("Exporter started - listening on :%s" % bind_port)
    while True:
        time.sleep(1)
