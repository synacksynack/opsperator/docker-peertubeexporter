FROM docker.io/python:3-slim-buster

# PeerTube Exporter image for OpenShift Origin

ARG DO_UPGRADE=
ENV DEBIAN_FRONTEND=noninteractive

LABEL io.k8s.description="Kubernetes PeerTube Prometheus Exporter Image." \
      io.k8s.display-name="Kubernetes PeerTube Prometheus Exporter" \
      io.openshift.expose-services="9113:http" \
      io.openshift.tags="prometheus,exporter,peertube" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-peertubeexporter" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="1.0.0"

WORKDIR /usr/src/app
COPY config/* /usr/src/app/

RUN set -x \
    && mv run-exporter.sh / \
    && apt-get update \
    && apt-get install -y dumb-init \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade Base Image"; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
    fi \
    && apt-get install -y curl \
    && pip install --no-cache-dir -r requirements.txt \
    && rm -rf /var/lib/apt/lists/* /usr/share/doc /usr/share/man \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

ENTRYPOINT [ "dumb-init", "--", "/run-exporter.sh" ]
USER 1001
